package com.example.demo;

import com.example.demo.controller.testController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;

@SpringBootApplication
public class DemoApplication {

    @Autowired
    testController tt;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }


}
